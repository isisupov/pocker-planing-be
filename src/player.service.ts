import {Player} from "./models/player.model";
import {randomUUID} from "crypto";

export class PlayerService {
    create(name: Player['name']): Player {
        return {
            id: randomUUID(),
            name
        }
    }
}
