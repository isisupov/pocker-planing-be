import {Controller, Get, Param, Post, Res} from "@nestjs/common";
import {randomUUID} from "crypto";

@Controller("game")
export class GameController {
    private static GAME_IDS: Array<string> = [];

    @Get(":id")
    getGameById(@Param() parameters: Record<string, any>, @Res() response): void {
        if(GameController.GAME_IDS.includes(parameters.id)) {
            response.status(200).send();
        }
        response.status(404).send();
    }

    @Post()
    createGame(): string {
        const newGameId: string = randomUUID();
        GameController.GAME_IDS.push(newGameId);
        return newGameId;
    }
}
