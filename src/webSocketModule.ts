import {Module} from '@nestjs/common';
import {WebSocketService} from "./webSocketService";
import {GameController} from "./gameController";
import {PlayerService} from "./player.service";

@Module({
    providers: [WebSocketService, PlayerService],
    controllers: [GameController]
})
export class WebSocketModule {
}
