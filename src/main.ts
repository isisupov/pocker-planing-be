import {NestFactory} from '@nestjs/core';
import {WebSocketModule} from './webSocketModule';
import {NestExpressApplication} from '@nestjs/platform-express';
async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(WebSocketModule, {cors: true});
    await app.listen(8080);
}

bootstrap();
