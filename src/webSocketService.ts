import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import {Logger} from '@nestjs/common';
import {Server, Socket} from 'socket.io';
import {SocketEvents} from "./models/socket.events";
import {PlayerService} from "./player.service";

@WebSocketGateway({ cors: true })
export class WebSocketService implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private static readonly POKER_WEBSOCKET_NAME = 'nc.poker.planing.socket';
    @WebSocketServer()
    server: Server;

    private logger: Logger = new Logger('WebSocketService');

    constructor(private playerService: PlayerService) {
    }

    @SubscribeMessage(WebSocketService.POKER_WEBSOCKET_NAME)
    handleMessage<T extends {eventName: SocketEvents, payload: unknown}>(client: Socket, payload: T): void {
        switch (payload.eventName) {
            case SocketEvents.CREATE_PLAYER: {
                this.logger.log("CREATE_PLAYER", payload);
                this.sendMessage({
                    eventName: SocketEvents.CREATE_PLAYER_SUCCESS,
                    payload: this.playerService.create(payload.payload as string)
                });
                break;
            }
        }
    }

    sendMessage(payload: unknown): boolean {
        return this.server.emit(WebSocketService.POKER_WEBSOCKET_NAME, payload);
    }

    afterInit(server: Server) {
        this.logger.log('Init');
    }

    handleDisconnect(client: Socket) {
        this.logger.log(`Client disconnected: ${client.id}`);
    }

    handleConnection(client: Socket, ...args: any[]) {
        this.logger.log(`Client connected: ${client.id}`);
    }
}
